function reloadGame (event) {
  if (event !== undefined && event.type === 'click') { // Reload sequence initialized by button press on informationScreen -> send reload request to gameScreen and unregister from room first
    MTLG.distributedDisplays.communication.sendCustomAction('room', 'reloadGame')
    MTLG.distributedDisplays.rooms.leaveRoom('room', function (result) {
      if (result && result.success) {
        setTimeout(reloadGame, 500)
      }
    })
  } else {
    location.reload()
  }
}

function controlBackgroundMusic (event) {
  const controlButton = event.target

  // Define background music at first button press
  if (controlButton.backgroundMusic === undefined) {
    controlButton.backgroundMusic = MTLG.assets.playSound('theme')
    controlButton.backgroundMusic.loop = -1 // loop infinitely
  }

  controlButton.playState = !controlButton.playState
  controlButton.backgroundMusic.paused = !controlButton.playState
  controlButton.image.src = 'img/play-' + controlButton.playState + '.png'
}

function createButton (x, y, imagePath, func) {
  const button = MTLG.assets.getBitmap(imagePath)
  button.regX = button.getBounds().width / 2
  button.regY = button.getBounds().height / 2
  button.set({
    x: x,
    y: y
  })
  button.shadow = new createjs.Shadow('#000000', 3, 3, 10)
  button.on('click', func)
  button.playState = false
  return button
}

module.exports = { createButton, reloadGame, controlBackgroundMusic }
