const gameElements = require('./gameElements.js')
const { monkeys } = require('./monkeys.js')
const controlElements = require('./controlElements.js')

function initScreen () {
  const stage = MTLG.getStageContainer()

  const background = MTLG.assets.getBitmap('img/background-bottom.png')
  background.set({
    x: 0,
    y: 0
  })

  // puzzle elements
  const inputData = gameElements.createGameElement('puzzleElement', 'img/start.png', 133, 644, 1, 'input')
  const outTrue1 = gameElements.createGameElement('puzzleElement', 'img/out-true.png', 1500, 125, 1, 'outTrue')
  const outTrue2 = gameElements.createGameElement('puzzleElement', 'img/out-true.png', 1750, 125, 1, 'outTrue')
  const outFalse1 = gameElements.createGameElement('puzzleElement', 'img/out-false.png', 1500, 250, 1, 'outFalse')
  const outFalse2 = gameElements.createGameElement('puzzleElement', 'img/out-false.png', 1750, 250, 1, 'outFalse')

  // filter elements
  const f1 = gameElements.createGameElement('filterElement', 'img/filter-auge.png', 1000, 500, 1, 'eyeOpen')
  const f2 = gameElements.createGameElement('filterElement', 'img/filter-accessoire.png', 1600, 500, 1, 'accessory')
  const f3 = gameElements.createGameElement('filterElement', 'img/filter-mund.png', 1300, 750, 1, 'mouthOpen')
  // const f4 = gameElements.createGameElement('filterElement', 'img/filter-laecheln.png', 1690, 900, 1, 'smiling')

  // Set input data
  inputData.data = monkeys

  stage.addChild(background, inputData, f1, f2, f3, outTrue1, outTrue2, outFalse1, outFalse2)

  // register custom function for processing messages from informationScreen
  MTLG.distributedDisplays.actions.setCustomFunction('reloadGame', controlElements.reloadGame)
}

module.exports = { initScreen }
